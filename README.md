## NIS Docker   

### Container for quick deployment of NIS  

The node or NIS is what you will want to run if you are trying to set up a supernode or a full node.  
https://nem.io/downloads/  

For start:  
Install Docker and run commands:  

```bash
docker pull openjdk  
```
Clone git repository   

```bash
git clone https://denja@bitbucket.org/denja/nis_docker.git
cd nis_docker
```
Go to the directory of interest  

For main net
```bash
cd mainnet 
```
For test net   
```bash
cd testnet  
```
Build docker  
```bash
docker build -t nis .  
```
Run docker  
```bash
docker run -v /var/log/nis:/root/nem/nis/logs -p 0.0.0.0:7890:7890 -p 0.0.0.0:7891:7891 -p 0.0.0.0:7778:7778 -d  -it --rm --name nis nis  
```
For donate:  

NA2HVX-BACOYN-WYE35U-GWXYXF-F5XYLT-6QQRMO-5WXC  

