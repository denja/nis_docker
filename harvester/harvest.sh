#!/bin/bash

if [[ $# -ne 1 ]]; then
	echo "you need to provide private key";
	exit 1
fi

key=`echo $1 | sed 's/[0-9a-fA-F]*//'`
if [[ -n "$key" ]];
then
	echo "provided key must be in hexadecimal format"
	exit 2
fi

echo "Attempt to start harvesting:"
sleep_time=60
host="localhost"
port=7890
# additional script { ----
until (curl 1s -s -H "Accept: application/json" -H "Content-Type: application/json" http://${host}:${port}/status | grep -ioE '"code":6' > /dev/null); do
  echo "Waiting for NIS to recieve request..."
  sleep ${sleep_time}s
done
# ---- }

curl -s -H "Accept: application/json" -H "Content-Type: application/json" -d "{'value':'$1'}" http://${host}:${port}/account/unlock
echo ""
echo "Successful enabling harvesting."

